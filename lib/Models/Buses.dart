
class Bus {
  String ?id;
  String ?busnumber;
  String ?numbofseats;
  String ?stopstations;
  String ?goofftime;
  String ?arrivetiome;
  String ?driver_id;
  String ?docid;

  Bus(
      { this.arrivetiome,
       this.busnumber,
       this.goofftime,
       this.docid,
       this.numbofseats,
       this.stopstations,
       this.driver_id,
       this.id
       });
  Bus.fromjson(Map<String, dynamic> json,)
  {
    id=json['id'];
    busnumber=json['bus_number'];
    numbofseats = json['num_of_seats'];
    stopstations = json['stop_stations'];
    goofftime = json['go_off_time'];
    arrivetiome = json['arrive_time'];
    driver_id = json['driver_id'];
  }
}
