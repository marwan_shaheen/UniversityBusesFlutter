class Admin {
  String? id;
  String? username;
  String? password;
  Admin({this.username, this.password, this.id});
  Admin.fromjson(Map<String, dynamic> json) {
    print('admin json $json');
    id = json['id'];
    username = json['username'];
    password = json['password'];
  }
}