class Student {
  String? id;
  String? studentname;
  String? username;
  String? password;
  String? docid;
  Student({this.studentname, this.username, this.password, this.id,this.docid});
  Student.fromjson(Map<String, dynamic> json) {
    id = json['id'];
    studentname = json['student_name'];
    username = json['username'];
    password = json['password'];
  }
}
