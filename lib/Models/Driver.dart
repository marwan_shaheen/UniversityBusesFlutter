class Driver {
  String? id;
  String? drivername;
  String? username;
  String? password;
  String? docid;
  Driver({this.drivername, this.username, this.docid, this.password, this.id});
  Driver.fromjson(Map<String, dynamic> json) 
  {
    id = json['id'];
    drivername = json['driver_name'];
    username = json['username'];
    password = json['password'];
  }
}
