import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sizer/sizer.dart';

class ShimmerWidgetGrid extends StatelessWidget {
  var width, height, itemcount, count;
  // Axis? h;
  ShimmerWidgetGrid(
      {super.key, this.height, this.width, this.itemcount, this.count});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: const Color.fromARGB(255, 203, 213, 225),
      highlightColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Container(
          height: height,
          child: GridView.count(
            crossAxisCount: count!,
            mainAxisSpacing: 8.h,
            // childAspectRatio: 0.57,
            crossAxisSpacing: 2.h,
            shrinkWrap: true,
            semanticChildCount: 6,
            physics: const NeverScrollableScrollPhysics(),
            children: List.generate(
              itemcount!,
              (index) => Column(
                children: [
                  Container(
                    // width: 20.w,
                    height: 20.h,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 5,
                          color: Colors.grey,
                          offset: Offset(0, 1),
                        ),
                      ],
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}