// ignore_for_file: must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Features/Buses/Pages/bus_details.dart';

class BusCard extends StatelessWidget {
  BusCard(
      {Key? key,
      required this.busNumber,
      required this.onEdit,
      required this.delete,
      required this.index})
      : super(key: key);
  String busNumber;
  int index;
  void Function() onEdit;
  void Function() delete;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => BusesDetails(index),
          )),
      child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
          ),
          // margin:const EdgeInsets.symmetric(vertical: 40),
          height: 130,
          width: MediaQuery.of(context).size.width * 4 / 9,
          child: Card(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: Row(children: [
                Expanded(
                    flex: 10,
                    child: Container(
                        color: const Color(0xFFE8ECF4),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(busNumber,
                                  style: const TextStyle(
                                      fontSize: 16,
                                      color: Color(0xff8391A1),
                                      fontFamily: 'Urbanist')),
                              // Text('$studentNumber Student',
                              //     style: const TextStyle(
                              //         fontSize: 16,
                              //         color: Color(0xff8391A1),
                              //         fontFamily: 'Urbanist')),
                              // Text(driverName,
                              //     style: const TextStyle(
                              //         fontSize: 16,
                              //         color: Color(0xff8391A1),
                              //         fontFamily: 'Urbanist'))
                            ]))),
                Expanded(
                    flex: 3,
                    child: Container(
                        color: const Color(0xff6E234E),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              IconButton(
                                onPressed: delete,
                                icon: const Icon(Icons.delete_outline,
                                    color: Colors.white, size: 20),
                              ),
                              IconButton(
                                onPressed: onEdit,
                                icon:const Icon(Icons.edit_road_sharp,
                                    color: Colors.white, size: 20),),
                            ])))
              ]))),
    );
  }
}
