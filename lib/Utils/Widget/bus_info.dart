import 'package:flutter/material.dart';

import '../../Models/Buses.dart';

class BusInfo extends StatelessWidget {
  const BusInfo({super.key,required this.bus});
  final Bus bus;

  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: "Bus Number: ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
              bus.busnumber!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: "Number of seats : ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: bus.numbofseats!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: "Stop Stations : ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: bus.stopstations!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: "Go-off time : ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:
              bus.goofftime!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: "Arrive time : ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: bus.arrivetiome!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
      ],
    );
  }
}
