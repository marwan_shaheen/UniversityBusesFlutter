import 'package:flutter/material.dart';

class CostumeTopBackButton extends StatelessWidget {
  const CostumeTopBackButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 30),
        Row(
            children: [
              Container(
                decoration: BoxDecoration(
                    border:Border.all(color: const Color(0xFFE8ECF4)),
                    borderRadius: BorderRadius.circular(16)
                ),
                margin:const EdgeInsets.symmetric(vertical: 15),
                child: IconButton(onPressed: (){
                  Navigator.pop(context);
                },
                    color: const Color(0xFF1E232C),
                    icon:const Icon(Icons.arrow_back_ios_rounded,
                        size: 20)),
              ),
              const Spacer()
            ]
        ),
        const SizedBox(height: 25)
      ]
    );
  }
}
