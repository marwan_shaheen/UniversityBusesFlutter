import 'package:flutter/material.dart';

import '../../Models/Student.dart';

class StudentInfo extends StatelessWidget {
  final Student student;
  const StudentInfo({super.key,required this.student});

  @override
  Widget build(BuildContext context) {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: " Student Name: ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:student.studentname!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: " Username: ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: student.username!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: "Password: ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: student.password!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
      ],
    );
  }
}
