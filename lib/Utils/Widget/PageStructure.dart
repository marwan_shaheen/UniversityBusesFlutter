// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:student_bus_project/Features/Student/Page/add_student.dart';

class PageStructure extends StatefulWidget {
  PageStructure(
      {Key? key,
      required this.body,
      required this.homeButton,
      this.flatingactionbutton})
      : super(key: key);
  Widget body;
  void Function() homeButton;
  Widget? flatingactionbutton;
  @override
  _PageStructureState createState() => _PageStructureState();
}

class _PageStructureState extends State<PageStructure> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // floatingActionButton:widget.flatingactionbutton,
        // bottomSheet: Container(
        //   height: 95,
        //   decoration: const BoxDecoration(
        //       // image: DecorationImage(
        //       //   image: AssetImage('asset/image/navigationBackGround.png'),
        //       // ),
        //       ),
        // ),
        floatingActionButton: widget.flatingactionbutton,
        // floatingActionButtonLocation:
        //     FloatingActionButtonLocation.miniCenterFloat,
        // floatingActionButton: Container(
        //     margin: const EdgeInsets.only(top: 140),
        //     child: FloatingActionButton(
        //         foregroundColor: Colors.white,
        //         backgroundColor: Colors.white,
        //         shape: RoundedRectangleBorder(
        //             borderRadius: BorderRadius.circular(50)),
        //         onPressed: widget.homeButton,
        //         child: Container(
        //             margin: const EdgeInsets.all(1),
        //             decoration: BoxDecoration(
        //                 color: const Color(0xFF6E234E),
        //                 borderRadius: BorderRadius.circular(50)),
        //             child: const Center(
        //                 child: Icon(Icons.home_filled,
        //                     color: Colors.white, size: 35))))),
        body: widget.body);
  }
}
// My11297$