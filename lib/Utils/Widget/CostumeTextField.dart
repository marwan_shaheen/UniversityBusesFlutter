import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CostumeTextField extends StatelessWidget {
  CostumeTextField(
      {required this.controller, required this.hint, Key? key,  this.validate})
      : super(key: key);
  TextEditingController controller;
  String hint;
  String? Function(String?)? validate;
  @override
  Widget build(BuildContext context) {
    return Container(
        color: const Color(0xFFF7F8F9),
        child: TextFormField(
          validator: validate,
            controller: controller,
            style: const TextStyle(
                color: Color(0xFF122121),
                fontWeight: FontWeight.w100,
                fontFamily: 'Urbanist'),
            autovalidateMode: AutovalidateMode.onUserInteraction,
            cursorColor: const Color(0xFF8391A1),
            decoration: InputDecoration(
                fillColor: const Color(0xFFF7F8F9),
                hintText: hint,
                hintStyle: const TextStyle(
                    color: Color(0xFF8391A1),
                    fontWeight: FontWeight.w100,
                    fontFamily: 'Urbanist'),
                border: OutlineInputBorder(
                    borderSide: const BorderSide(color: Color(0xFFE8ECF4)),
                    borderRadius: BorderRadius.circular(8)),
                focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Color(0xFFE8ECF4)),
                    borderRadius: BorderRadius.circular(8)),
                enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Color(0xFFE8ECF4)),
                    borderRadius: BorderRadius.circular(8)))));
  }
}
