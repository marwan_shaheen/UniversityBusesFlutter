import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:student_bus_project/Features/Driver/Page/driver_details.dart';
import 'package:student_bus_project/Features/Student/Page/student_details.dart';

// ignore: must_be_immutable
class StudentAndDriverCard extends StatelessWidget {
  StudentAndDriverCard(
      {Key? key,
      this.busNumber,
      required this.studentName,
      required this.delete,
      required this.index,
      this.isDriverPage,
      required this.onEdit,required this.id})
      : super(key: key);
  String? studentName, busNumber;
  void Function() delete;
  void Function() onEdit;
  int index;
  bool? isDriverPage = false;
  String id;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (isDriverPage == true) {
          print('driver page');
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => DriverDetails(id: id,)));
        } 
        else 
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => StudentDetails(id: id,)));
        }
      },
      child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
          ),
          // margin:const EdgeInsets.symmetric(vertical: 40),
          height: 150,
          width: MediaQuery.of(context).size.width * 4 / 9,
          child: Card(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: Column(children: [
                Expanded(
                  flex: 5,
                  child: Container(
                    color: const Color(0xFFE8ECF4),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Center(
                              child: Text(studentName!,
                                  style: const TextStyle(
                                      fontSize: 16,
                                      color: Color(0xff8391A1),
                                      fontFamily: 'Urbanist'))),
                          // Center(
                          //     child: Text(busNumber,
                          //         style: const TextStyle(
                          //             fontSize: 16,
                          //             color: Color(0xff8391A1),
                          //             fontFamily: 'Urbanist'))),
                        ]),
                  ),
                ),
                Expanded(
                    flex: 2,
                    child: Container(
                        color: const Color(0xff6E234E),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              IconButton(
                                onPressed: delete,
                                icon: Icon(Icons.delete_outline,
                                    color: Colors.white, size: 20),
                              ),
                              IconButton(
                                onPressed: onEdit,
                                icon: Icon(Icons.edit_road_sharp,
                                    color: Colors.white, size: 20),
                              ),
                            ])))
              ]))),
    );
  }
}
