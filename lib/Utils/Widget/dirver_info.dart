import 'package:flutter/material.dart';

import '../../Models/Driver.dart';

class DriverInfo extends StatelessWidget {
  final Driver driver;
  const DriverInfo({super.key,required this.driver});

  @override
  Widget build(BuildContext context) {
    return  Column(

      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: " Driver name : ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:driver.drivername!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: " Username : ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:driver!.username!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
        RichText(
          text: TextSpan(children: [
            const TextSpan(
              text: " password : ",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text:driver.password!,
              style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Urbanist',
                  fontWeight: FontWeight.bold),
            ),
          ]),
        ),
      ],
    );
  }
}
