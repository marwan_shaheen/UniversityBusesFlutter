import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PrimaryButton extends StatelessWidget {
  PrimaryButton({Key? key, required this.label, required this.onPressed})
      : super(key: key);
  String label;
  void Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: const Color(0xFF6E234E),
          borderRadius: BorderRadius.circular(8)),
      child: TextButton(
        style: ButtonStyle(),
        onPressed: onPressed,
        child: Container(
            width: 200,
            height: 40,
            // margin: const EdgeInsets.symmetric(vertical: 10),
            child: Center(
                child:
                    Text(label, style: const TextStyle(color: Colors.white)))),
      ),
    );
  }
}
