import 'package:flutter/material.dart';

import '../Features/Home/Page/home_page.dart';

void goToHome(BuildContext context){
  Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage(),), (route) => false);
}