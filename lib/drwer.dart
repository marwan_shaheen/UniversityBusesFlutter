// ignore_for_file: prefer_const_constructors, must_be_immutable

import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:student_bus_project/Core/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:student_bus_project/Features/Login/Cubit/auth_state.dart';
import 'package:student_bus_project/Features/Splash/Page/login_role.dart';
import 'package:student_bus_project/Utils/Widget/bus_info.dart';
import 'package:student_bus_project/Utils/Widget/dirver_info.dart';
import 'package:student_bus_project/Utils/Widget/student_info.dart';

import 'package:student_bus_project/remote/Cach_Helper.dart';

import 'Features/Login/Cubit/cubit.dart';
import 'Features/Student/Cubit/states.dart';

class MyDrawer extends StatelessWidget {
  MyDrawer({required this.scaffoldKey, super.key, required this.collection});
  String? collection;
  GlobalKey<ScaffoldState> scaffoldKey;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => AuthCubit()
          ..getProfile(
              collection: collection, id: AppSharedPreferences.id),
        child: BlocConsumer<AuthCubit, OpStates>(
          listener: (context, state) {
            if(state is LogoutSuccessfulState){
              Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => Welcome(),), (route) => false);
            }
          },
          builder: (context, state) {
            var cubit = AuthCubit.get(context);
            return Drawer(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                  children: [
                Column(
                  children: [
                    SizedBox(height: 4.h),
                    SizedBox(
                        height: MediaQuery.of(context).size.height / 7,
                        child: const Image(
                            image: AssetImage('asset/image/logo.png'),
                            fit: BoxFit.scaleDown)),
                    SizedBox(height: 3.h),
                    state is LoadingState ?CircularProgressIndicator():state is ErrorState ?SizedBox():
                    collection == 'drivers'?DriverInfo(driver: cubit.driver!):
                    collection == 'students' ? StudentInfo(student: cubit.student!):
                        Text(cubit.admin!.username!)

                  ],
                ),


                TextButton(onPressed: (){

                  context.read<AuthCubit>().logout();

                }, child: Text('logout'))
              ],
            ));
          },
        ));
  }
}
