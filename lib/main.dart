import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:sizer/sizer.dart';
import 'package:student_bus_project/Core/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:student_bus_project/Features/Driver/Page/driver.dart';
import 'package:student_bus_project/Features/Home/Page/home_page.dart';
import 'package:student_bus_project/Features/Splash/Page/splash.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:student_bus_project/Features/Buses/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Driver/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Splash/Page/login_role.dart';
import 'package:student_bus_project/Features/Student/Page/student.dart';
import 'package:student_bus_project/firebase_options.dart';
import 'package:student_bus_project/remote/Cach_Helper.dart';

import 'Features/Tracking/Cubit/cubit.dart';
import 'Features/Student/Cubit/cubit.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  FirebaseFirestore.instance.settings = const Settings(
    persistenceEnabled: true,
  );
  await AppSharedPreferences.init();
  await CachHelper.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => OpStudentsCubit()..getStudents()),
          BlocProvider(create: (_) => OpDriverCubit()..getDrivers()),
          BlocProvider(create: (_) => OpBussesCubit()..getBusesEvent()),
          BlocProvider(create: (_) => TrackOnMapCubit()),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Buses',
          locale: const Locale('ar'),
          supportedLocales: const [Locale('ar')],
          theme: ThemeData(
              bottomSheetTheme: const BottomSheetThemeData(
                  backgroundColor: Colors.transparent, elevation: 0)),
          home: AppSharedPreferences.hasPassword == false ?
          const Welcome():AppSharedPreferences.getRole == 'admin'? HomePage():
          AppSharedPreferences.getRole == 'students'? const StudentPage(): DriverPage(),
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate
          ],
        ),
      );
    });
  }
}
