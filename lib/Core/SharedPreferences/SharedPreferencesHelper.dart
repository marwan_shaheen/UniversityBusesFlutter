import 'package:shared_preferences/shared_preferences.dart';

import 'SharedPreferencesProvider.dart';

class AppSharedPreferences {
  static SharedPreferencesProvider? sharedPreferencesProvider;
  static late SharedPreferences preferences;
  static init() async {
    sharedPreferencesProvider = await SharedPreferencesProvider.getInstance();
    preferences = await SharedPreferences.getInstance();
  }


  //password
  static String get getPassword =>
      sharedPreferencesProvider!.read('password') ?? '';
  static void savePassword(String value) =>
      sharedPreferencesProvider!.save('password', value);
  static bool get hasPassword =>
      sharedPreferencesProvider!.contains('password');
  static removePassword() => sharedPreferencesProvider!.remove('password');


  //username
  static String get username => sharedPreferencesProvider!.read('username') ?? '';
  static void saveUsername(String value) =>
      sharedPreferencesProvider!.save('username', value);
  static bool get hasUsername =>
      sharedPreferencesProvider!.contains('username');
  static removeUsername() => sharedPreferencesProvider!.remove('username');

  //role
  static String get getRole =>
      sharedPreferencesProvider!.read('role') ?? '';
  static void saveRole(String value) =>
      sharedPreferencesProvider!.save('role', value);
  static bool get hasRole =>
      sharedPreferencesProvider!.contains('role');
  static removeRole() => sharedPreferencesProvider!.remove('role');

  //busNumber
  static String get id =>
      sharedPreferencesProvider!.read('id') ?? '';
  static void saveId(String value) =>
      sharedPreferencesProvider!.save('id', value);
}
