import 'package:cloud_firestore/cloud_firestore.dart';

class FireStoreHelper {

  static Future<DocumentReference<Map<String, dynamic>>> postDocument(
      String path, var document) async {
    var db = FirebaseFirestore.instance;
    return await db.collection(path).add(document);
  }

  static Future<QuerySnapshot<Map<String, dynamic>>> getDocuments(String path,Map<String,dynamic>? filter) async {
    var db = FirebaseFirestore.instance;
    return await db.collection(path).get();
  }

  static Future<DocumentSnapshot<Map<String, dynamic>>>
      getInformationInDocument({String ?path, String ?documentId}) async {
    var db = FirebaseFirestore.instance;
    return await db.collection(path!).doc(documentId).get();
  }

  static Future<QuerySnapshot<Map<String, dynamic>>> searchDocuments(
    String path,
    String field1,
    String username,
    String field2,
    String password,
  ) async {
    var db = FirebaseFirestore.instance;
    return await db
        .collection(path)
        .where(field1, isEqualTo: username)
        .where(field2, isEqualTo: password)
        .get();
  }

  static Future<QuerySnapshot<Map<String, dynamic>>> getFilteredDocuments(
      String path,
      Map<String,dynamic>? filter,

      ) async {
    var db = FirebaseFirestore.instance;
    Query<Map<String, dynamic>> query = db.collection(path);
    if(filter != null) {
      filter.forEach((key, value) {
        query = query.where(key, isEqualTo: value);
      });
    }
    return await query.get();
  }

  static Future<void> deleteDocument(String path, String documentId) async {
    var db = FirebaseFirestore.instance;
    await db.collection(path).doc(documentId).delete();
  }

  static Future<void> updateDocument(
      String collectionName, String documentId, var data) async {
    await FirebaseFirestore.instance
        .collection(collectionName)
        .doc(documentId)
        .update(data);
  }


}
