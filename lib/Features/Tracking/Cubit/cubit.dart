import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:sizer/sizer.dart';
import 'package:student_bus_project/Core/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:student_bus_project/Core/firestore_helper.dart';
import 'package:student_bus_project/Features/Buses/Cubit/cubit.dart';

import '../../../Models/Buses.dart';
import '../../Student/Cubit/states.dart';

import 'package:firebase_database/firebase_database.dart';
import 'dart:ui' as ui;



class TrackOnMapCubit extends Cubit<OpStates> {
  TrackOnMapCubit() : super(OpInitialState());
  static TrackOnMapCubit get(context) => BlocProvider.of(context);
  Location location = Location();
  List<Bus> buses = [];
  Set<Marker> markers = {};

  BitmapDescriptor? markerIcon;
  OpBussesCubit opBussesCubit = OpBussesCubit();


  final Completer<GoogleMapController> controller = Completer();
  String? latlong = '';

  Future<String?> getLatLong() {
    return determinePosition().then((position) {
      print('position result $position');
      String? lat = position.latitude.toString();
      String? long = position.longitude.toString();
      String? latlong = '$lat, $long';
      return latlong;
    }).catchError((error) {
      print(error);
    });
  }

  Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission? permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }

  String? latLong;

  Future<void> getAndSetLatLong() async {
    String? result = await getLatLong();
    if (result != null) {
      latLong = result;
    }
    emit(GetLatLongStateSuccess());
  }


  LatLng parseLatLng(String latLong) {
    print('latLong $latlong');
    List<String> latLongList = latLong.split(', ');
    double lat = double.parse(latLongList[0]);
    double lng = double.parse(latLongList[1]);
    return LatLng(lat, lng);
  }


  void onLocationChanged()async{
    try {
      List<Bus> buses2 = await opBussesCubit.getBuses(filter: {'driver_id':AppSharedPreferences.id});
      if(buses2.isNotEmpty) {
        location.onLocationChanged.listen((locationData) {
          saveLocation(locationData.latitude!, locationData.longitude!,buses2.first);
        });
      }
    }catch(error){
    }

  }

  void saveLocation(double lat, double lng, Bus bus)async{
    FirebaseDatabase.instance.ref('buses').child(bus.docid!).set({
      'lat': lat,
      'lng': lng,
    });

  }
  void trackBuses()async{
    markerIcon ??= BitmapDescriptor.fromBytes(await getBytesFromAsset('asset/image/school-bus_2554935.png',(5.w * 10).round()));

    FirebaseDatabase database = FirebaseDatabase.instance;

    database.ref('buses').onValue.listen((event) {
       buses.clear();
       markers.clear();
        event.snapshot.children.forEach((element) async{

            buses.add(Bus(driver_id: element.key!));

            markers.add(Marker(position:LatLng(
                (element.value as Map)['lat'],(element.value as Map)['lng'])
                ,markerId: MarkerId(element.key!),
                icon: markerIcon!,
              onTap: ()async{
                emit(GetTrackedBusLoading());
                try {
                  Bus? bus = await opBussesCubit.getOneBus(element.key!);
                  if(bus != null){
                    emit(GetTrackedBusSuccess(bus: bus));
                  }else{
                    emit(GetTrackedBusError());
                  }
                }catch(error){
                  emit(GetTrackedBusError());
                }


              }
            ));
        });
        emit(SuccessState());
    });
  }




  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!.buffer.asUint8List();
  }


}
