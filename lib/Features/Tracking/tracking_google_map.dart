import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sizer/sizer.dart';
import 'package:student_bus_project/Utils/Widget/bus_info.dart';

import '../../Utils/Dialogs/loader_dialog.dart';
import '../Student/Cubit/states.dart';
import 'Cubit/cubit.dart';
enum TrackType{
  tracker, tracked
}
class TrackingGoogleMap extends StatefulWidget {
  final TrackType trackType;
  const TrackingGoogleMap ({super.key, required this.trackType});

  @override
  State<TrackingGoogleMap> createState() => _TrackingGoogleMapState();
}

class _TrackingGoogleMapState extends State<TrackingGoogleMap> {
  TrackOnMapCubit trackOnMapCubit = TrackOnMapCubit();
  @override

  @override
  Widget build(BuildContext context) {
    return  BlocProvider(
      create: (context) => trackOnMapCubit..getAndSetLatLong(),
      child: BlocConsumer<TrackOnMapCubit, OpStates>(
        listener: (context, state) {
          if(state is GetLatLongStateSuccess){

            if(widget.trackType == TrackType.tracked) {
              context.read<TrackOnMapCubit>().onLocationChanged();

            }else{
              context.read<TrackOnMapCubit>().trackBuses();
            }
          }

          if(state is GetTrackedBusSuccess){
            Navigator.pop(context);

            showDialog(context: context, builder: (context) => AlertDialog(
              content: SizedBox(
                height: 20.h,
                  child: BusInfo(bus: state.bus,)),
            ),);
          }

          if(state is GetTrackedBusLoading){
            showLoaderDialog(context);
          }
          if(state is GetTrackedBusError){
            Navigator.pop(context);
          }
        },
        builder: (context, state) {
          var cubit = TrackOnMapCubit.get(context);
          return cubit.latLong != null
              ? GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
                target: cubit.parseLatLng(cubit.latLong!), zoom: 10),
            onMapCreated: (GoogleMapController controller) {
              cubit.controller.complete(controller);
            },
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
            markers: context.read<TrackOnMapCubit>().markers,
          )
              : const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
