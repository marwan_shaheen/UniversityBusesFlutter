import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:student_bus_project/Features/Login/Page/login.dart';
import 'package:student_bus_project/Utils/Widget/PrimaryBottun.dart';

class Welcome extends StatelessWidget {
  const Welcome({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
            const SizedBox(height: 50),
            SizedBox(
                height: MediaQuery.of(context).size.height / 7,
                child: const Image(
                    image: AssetImage('asset/image/logo.png'),
                    fit: BoxFit.scaleDown)),
            Column(mainAxisAlignment: MainAxisAlignment.end, children: [
              const SizedBox(height: 30),
              const Text('اختر طريقة التسجيل',
                  style: TextStyle(
                      fontFamily: 'Urbanist',
                      fontSize: 20,
                      fontWeight: FontWeight.bold)),
              const SizedBox(height: 50),
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: PrimaryButton(
                      label: 'الطالب',
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>const  LogInPage("students"),
                          ),
                        );
                      })),
              const SizedBox(
                height: 50,
              ),
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: PrimaryButton(
                      label: 'السائق',
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const LogInPage("drivers"),
                          ),
                        );
                      })),
              const SizedBox(height: 50),
              Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: PrimaryButton(
                      label: 'المدير',
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LogInPage("admin"),
                          ),
                        );
                      })),
            ]),
            const SizedBox(
              height: 90,
            )
          ]))),
      // homeButton: () {},
    );
  }
}
