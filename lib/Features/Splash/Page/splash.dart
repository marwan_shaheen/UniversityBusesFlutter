
import 'package:flutter/material.dart';
import 'package:student_bus_project/Features/Login/Page/login.dart';
import 'package:student_bus_project/Features/Splash/Page/login_role.dart';
import 'package:student_bus_project/Utils/Widget/PrimaryBottun.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:const BoxDecoration(
          gradient:LinearGradient(
              begin:Alignment.topCenter,
              end:Alignment.bottomCenter,
              colors:[Color(0x886E234E),
            Colors.white,Colors.white,Colors.white])),
        child:  Stack(
          children: [
            const Positioned(
              left: 0,
                bottom: 0,
                child:SizedBox(child: Image(image: AssetImage('asset/image/downBackGround.png')))),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(
                height: 250,child: Image(image: AssetImage('asset/image/basImage.png'),fit: BoxFit.scaleDown)),
                const  SizedBox(height: 20),
                const SizedBox(
                    height: 100,
                    child: Image(image: AssetImage('asset/image/splashSecoundImage.png'))),
                const  SizedBox(height: 20),
                Container(
                    margin:const EdgeInsets.symmetric(horizontal: 20),
                    child: PrimaryButton(label: 'تسجيل', onPressed: (){
                      Navigator.push(context,MaterialPageRoute(builder:(_)=> const Welcome()));
                    }))
              ]
            )
          ]
        )
      )
    );
  }
}
