import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:student_bus_project/Features/Driver/Page/drivers_list.dart';
import 'package:student_bus_project/Features/Home/Page/home_page.dart';
import 'package:student_bus_project/Features/Driver/Cubit/cubit.dart';
import 'package:student_bus_project/Utils/Widget/CostumeTextField.dart';
import 'package:student_bus_project/Utils/Widget/CostumeTopBackBottum.dart';
import 'package:student_bus_project/Utils/Widget/PageStructure.dart';
import 'package:student_bus_project/Utils/Widget/PrimaryBottun.dart';
import 'package:student_bus_project/Utils/Widget/SnackBar.dart';

import '../../../Utils/helper.dart';
import '../../Student/Cubit/states.dart';



class AddDriverPage extends StatefulWidget {
  const AddDriverPage({super.key});

  @override
  State<AddDriverPage> createState() => _AddDriverPageState();
}

class _AddDriverPageState extends State<AddDriverPage> {
  TextEditingController driverNameController = TextEditingController();
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController cpasswordController = TextEditingController();
  var formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return PageStructure(
        body: BlocProvider(
          create: (context) => OpDriverCubit(),
          child: BlocConsumer<OpDriverCubit, OpStates>(
            listener: (context, state) {
              if (state is SuccessState) {
                ScaffoldMessenger.of(context).showSnackBar(ShowToastt(
                    text: 'تمت الإضافة بنجاح', state: ToastStates.Succes));
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ViewAllDrivers(),
                  ),
                );
              }
            },
            builder: (context, state) {
              var cubit = OpDriverCubit.get(context);
              return Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: SingleChildScrollView(
                      child: Form(
                    key: formkey,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          CostumeTopBackButton(),
                          const Text('إضافة سائق',
                              style: TextStyle(
                                  fontSize: 22,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.bold)),
                          SizedBox(height: 35),
                          CostumeTextField(
                            controller: driverNameController,
                            hint: 'اسم السائق',
                            validate: (value) {
                              if (value!.isEmpty) {
                                return "اسم السائق مطلوب";
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 20),
                          CostumeTextField(
                            controller: userNameController,
                            hint: 'اسم المستخدم',
                            validate: (value) {
                              if (value!.isEmpty) {
                                return "اسم المستخدم مطلوب";
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 20),
                          CostumeTextField(
                            controller: passwordController,
                            hint: 'كلمة المرور مطلوبة',
                            validate: (value) {
                              if (value!.isEmpty) {
                                return "كلمة المرور مطلوبة";
                              } else if (value!.isEmpty || value.length < 8) 
                              {
                                return "يجب أن لا يقل طول كلمة المرور عن 8";
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 20),
                          CostumeTextField(
                            controller: cpasswordController,
                            hint: 'تأكيد كلمة المرور',
                            validate: (value) {
                              if (value != passwordController.text)
                                return "كلمات المرور غير متطابقة ";
                              return null;
                            },
                          ),
                          SizedBox(height: 50),
                          ConditionalBuilder(
                            condition: state is! LoadingState,
                            builder: (context) => PrimaryButton(
                                label: 'إضافة',
                                onPressed: () {
                                  if (formkey.currentState!.validate()) {
                                    cubit.addDriver({
                                      'id': '',
                                      'driver_name': driverNameController.text,
                                      'username': userNameController.text,
                                      'password': passwordController.text
                                    });
                                  }
                                }),
                            fallback: (context) => Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                          SizedBox(height: 95)
                        ]),
                  )));
            },
          ),
        ),
        homeButton: () {
          goToHome(context);
        });
  }
}
