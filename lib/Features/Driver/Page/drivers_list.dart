import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:student_bus_project/Features/Driver/Page/add_driver.dart';
import 'package:student_bus_project/Features/Home/Page/home_page.dart';
import 'package:student_bus_project/Features/Driver/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Driver/Page/UpdateDriver/updatedriver.dart';
import 'package:student_bus_project/Utils/Widget/PageStructure.dart';
import 'package:student_bus_project/Utils/Widget/Shimmer.dart';
import 'package:student_bus_project/Utils/Widget/StudentCard.dart';

import '../../../Utils/helper.dart';
import '../../Student/Cubit/states.dart';


class ViewAllDrivers extends StatefulWidget {
  const ViewAllDrivers({super.key});

  @override
  State<ViewAllDrivers> createState() => _ViewAllDriversState();
}

class _ViewAllDriversState extends State<ViewAllDrivers> {
  @override
  Widget build(BuildContext context) {
    return PageStructure(
        flatingactionbutton: Positioned(
            bottom: 100,
            right: 20,
            child: FloatingActionButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
                heroTag: 'second',
                backgroundColor: Color(0xFFDB946E),
                child: Icon(Icons.add, color: Colors.white),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (_) => AddDriverPage()));
                })),
        body: BlocProvider(
          create: (context) => OpDriverCubit()..getDrivers(),
          child: BlocConsumer<OpDriverCubit, OpStates>(
            listener: (context, state) {},
            builder: (context, state) {
              var cubit = OpDriverCubit.get(context);

              return Stack(children: [
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 30),
                        Row(children: [
                          Container(
                            decoration: BoxDecoration(
                                border:
                                    Border.all(color: const Color(0xFFE8ECF4)),
                                borderRadius: BorderRadius.circular(16)),
                            margin: const EdgeInsets.symmetric(vertical: 15),
                            child: IconButton(
                                onPressed: () {
                                  goToHome(context);
                                },
                                color: const Color(0xFF1E232C),
                                icon: const Icon(Icons.arrow_back_ios_rounded,
                                    size: 20)),
                          ),
                          const Spacer()
                        ]),
                        const SizedBox(height: 25),
                        const Text('السائقين',
                            style: TextStyle(
                                fontSize: 22,
                                fontFamily: 'Urbanist',
                                fontWeight: FontWeight.bold)),
                        state is LoadingState
                            ? ShimmerWidgetGrid(
                                count: 2,
                                width: 35.w,
                                height: 80.h,
                                itemcount: 4,
                              )
                            : state is ErrorState
                                ? SizedBox()
                                : GridView.builder(
                                    shrinkWrap: true,
                                    physics: const ScrollPhysics(),
                                    gridDelegate:
                                        const SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisSpacing: 10,
                                            mainAxisSpacing: 10,
                                            childAspectRatio: 5 / 4,
                                            crossAxisCount: 2),
                                    itemCount: cubit.drivers.length,
                                    itemBuilder: (context, index) {
                                      return StudentAndDriverCard(
                                        id: cubit.drivers[index].docid!,
                                        isDriverPage: true,
                                          studentName:
                                              cubit.drivers[index].drivername,
                                          delete: () {
                                            setState(() {
                                              cubit
                                                  .deleteDriver(cubit
                                                      .drivers[index].docid!)
                                                  .then((value) {
                                                cubit.drivers.removeAt(index);
                                              });
                                            });
                                          },
                                          onEdit: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    UpdateDriver(
                                                  collectionname: 'drivers',
                                                  docid: cubit
                                                      .drivers[index].docid!,
                                                ),
                                              ),
                                            );
                                          }, index: index,);
                                    }),
                        const SizedBox(height: 90)
                      ],
                    ),
                  ),
                ),
              ]);
            },
          ),
        ),
        homeButton: () {
          Navigator.pop(context);
        });
  }
}
