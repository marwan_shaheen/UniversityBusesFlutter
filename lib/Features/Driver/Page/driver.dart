import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:student_bus_project/drwer.dart';

import '../../Tracking/tracking_google_map.dart';

class DriverPage extends StatelessWidget {
  DriverPage({super.key});
  final GlobalKey<ScaffoldState> globalKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      backgroundColor: Colors.white,
      key: globalKey,
      endDrawer: MyDrawer(scaffoldKey: globalKey, collection: 'drivers'),
      appBar: AppBar(
        title: const Text(
          'صفحة السائق',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
        actions: [
          IconButton(
            onPressed: () {
              globalKey.currentState!.openEndDrawer();
            },
            icon:const Icon(Icons.person_2_sharp,
                          size: 30,
                          color: Colors.black,
                        ),
          ),
        ],
      ),
      body:  const TrackingGoogleMap(trackType: TrackType.tracked,),
    );
  }
}
