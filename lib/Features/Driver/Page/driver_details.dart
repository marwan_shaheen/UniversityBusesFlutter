import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:student_bus_project/Features/Buses/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Driver/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Student/Cubit/cubit.dart';

import '../../Student/Cubit/states.dart';

class DriverDetails extends StatelessWidget {
  final String id;
  const DriverDetails({required this.id,super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => OpDriverCubit()..getOneDriver(id),
        child: BlocConsumer<OpDriverCubit, OpStates>(
          listener: (context, state) {},
          builder: (context, state) {
            var cubit = OpDriverCubit.get(context);
            return SafeArea(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 30),
                        Row(children: [
                          Container(
                            decoration: BoxDecoration(
                                border:
                                    Border.all(color: const Color(0xFFE8ECF4)),
                                borderRadius: BorderRadius.circular(16)),
                            margin: const EdgeInsets.symmetric(vertical: 15),
                            child: IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                color: const Color(0xFF1E232C),
                                icon: const Icon(Icons.arrow_back_ios_rounded,
                                    size: 20)),
                          ),
                          const Spacer()
                        ]),
                        const SizedBox(height: 25),
                        state is LoadingState
                            ? const Center(child: CircularProgressIndicator())
                            : state is ErrorState
                                ? const SizedBox()
                                : Container(
                                    margin: const EdgeInsets.symmetric(
                                      horizontal: 5,
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 25),
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(18),
                                      color: const Color(0xFFE8ECF4),
                                    ),
                                    child: Column(

                                      // crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        RichText(
                                          text: TextSpan(children: [
                                            const TextSpan(
                                              text: "اسم السائق: ",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            TextSpan(
                                              text: cubit
                                                  .driver!.drivername!,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ]),
                                        ),
                                        RichText(
                                          text: TextSpan(children: [
                                            const TextSpan(
                                              text: "اسم المستخدم: ",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            TextSpan(
                                              text: cubit
                                                  .driver!.username!,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ]),
                                        ),
                                        RichText(
                                          text: TextSpan(children: [
                                            const TextSpan(
                                              text: "كلمة المرور: ",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            TextSpan(
                                              text: cubit
                                                  .driver!.password!,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ]),
                                        ),
                                      ],
                                    ),
                                  ),
                        // const SizedBox(
                        //   height: 20,
                        // ),
                        // SizedBox(
                        //     height: 400,
                        //     child: ListView.builder(
                        //       itemCount: 5,
                        //       itemBuilder: (context, index) {
                        //         return Padding(
                        //           padding:
                        //               const EdgeInsets.symmetric(vertical: 10),
                        //           child: Text(
                        //             "Student ${index + 1}",
                        //             style: const TextStyle(fontSize: 20),
                        //           ),
                        //         );
                        //       },
                        //     ))
                      ]),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
