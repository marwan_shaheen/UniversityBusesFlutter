import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:student_bus_project/Core/firestore_helper.dart';

import 'package:student_bus_project/Models/Driver.dart';

import '../../Student/Cubit/states.dart';

class OpDriverCubit extends Cubit<OpStates> {
  OpDriverCubit() : super(OpInitialState());
  static OpDriverCubit get(context) => BlocProvider.of(context);

  Future<void> addDriver(Map<String, dynamic> driver) async {
    try {
      emit(LoadingState());
      await FireStoreHelper.postDocument('drivers', driver);
      emit(SuccessState());
    } catch (error) {
      print('hello error $error');
      emit(ErrorState(error.toString()));
    }
  }

  List<Driver> drivers = [];
  Driver? driver;
  Future<void> getDrivers() async {
    try {
      emit(LoadingState());

      final response = await FireStoreHelper.getDocuments('drivers');
      for (var element in response.docs) {
      var driver = Driver.fromjson(element.data());
      driver.docid = element.id; 
      drivers.add(driver);
    }
      emit(SuccessState());
    } catch (error) {
      emit(ErrorState(error.toString()));
    }
  }

  void getOneDriver(String id)async{
    try {
      emit(LoadingState());
      final response = await FireStoreHelper.getInformationInDocument(path:'drivers',documentId: id);
      driver = Driver.fromjson(response.data()!);

      emit(SuccessState());
    } catch (error) {
      print('driver response ${error}');
      emit(ErrorState(error.toString()));
    }
  }
   Future<void> deleteDriver(String documentId) async {
    try {
      emit(LoadingState());
      await FireStoreHelper.deleteDocument('drivers', documentId);
      emit(SuccessState());
    } catch (error) {
      print('hello error $error');
      emit(ErrorState(error.toString()));
    }
  }
}
