import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:student_bus_project/Features/Driver/Page/drivers_list.dart';
import 'package:student_bus_project/Features/Buses/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Driver/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Student/Page/students_list.dart';
import 'package:student_bus_project/Features/Buses/Pages/UpdateBus/updatebus.dart';
import 'package:student_bus_project/Features/Driver/Page/UpdateDriver/updatedriver.dart';
import 'package:student_bus_project/Features/Student/Page/updatestudent.dart';
import 'package:student_bus_project/Utils/Widget/BusCard.dart';
import 'package:student_bus_project/Utils/Widget/Shimmer.dart';
import 'package:student_bus_project/Utils/Widget/StudentCard.dart';
import 'package:student_bus_project/drwer.dart';

import '../../Buses/Pages/buses.dart';
import '../../Student/Cubit/cubit.dart';
import '../../Student/Cubit/states.dart';

class HomePage extends StatefulWidget {
  // HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey<ScaffoldState> globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: globalKey,
      endDrawer: MyDrawer(scaffoldKey: globalKey, collection: 'admin'),
      body: MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => OpStudentsCubit()..getStudents()),
          BlocProvider(create: (_) => OpDriverCubit()..getDrivers()),
          BlocProvider(create: (_) => OpBussesCubit()..getBusesEvent()),
        ],
        child: Container(
            child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SingleChildScrollView(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SizedBox(
                    height: 4.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "               ",
                        style: TextStyle(
                          fontSize: 15.sp,
                          fontWeight: FontWeight.bold,
                          // color: Colors.white
                        ),
                      ),
                      // SizedBox(
                      //   width: 25.w,
                      // ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height / 7,
                          child: const Image(
                              image: AssetImage('asset/image/logo.png'),
                              fit: BoxFit.scaleDown)),
                      const SizedBox(height: 30),
                      IconButton(
                        onPressed: () {
                          globalKey.currentState!.openEndDrawer();
                        },
                        icon: Icon(
                          Icons.person_2_sharp,
                          size: 30,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),

                  SizedBox(height: 2.h),
                  Column(children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text('الطلاب',
                              style: TextStyle(
                                  fontFamily: 'Urbanist',
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold)),
                          InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => ViewAllStudentPage()));
                              },
                              child: const Text('عرض الكل',
                                  style: TextStyle(
                                      fontFamily: 'Urbanist',
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xFF6E234E),
                                      decoration: TextDecoration.underline)))
                        ]),
                    const SizedBox(height: 10),
                    BlocBuilder<OpStudentsCubit, OpStates>(
                      builder: (context, state) {
                        var cubit = OpStudentsCubit.get(context);
                        return state is LoadingState
                            ? ShimmerWidgetGrid(
                                count: 2,
                                width: 35.w,
                                height: 20.h,
                                itemcount: 2,
                              )
                            : state is ErrorState
                                ? SizedBox()
                                : cubit.students.length == 0
                                    ? const Center(
                                        child: Text('لا يوجد طلاب',
                                            style: TextStyle(
                                                fontSize: 25,
                                                fontFamily: 'Urbanist',
                                                fontWeight: FontWeight.bold)),
                                      )
                                    : GridView.builder(
                                        shrinkWrap: true,
                                        physics: const ScrollPhysics(),
                                        gridDelegate:
                                            const SliverGridDelegateWithFixedCrossAxisCount(
                                                crossAxisSpacing: 10,
                                                mainAxisSpacing: 10,
                                                crossAxisCount: 2),
                                        itemCount: cubit.students.length,
                                        itemBuilder: (context, index) {
                                          return StudentAndDriverCard(
                                              id: cubit.students[index].docid!,
                                              studentName:
                                                  '${cubit.students[index].studentname}',
                                              delete: () {
                                                setState(() {
                                                  cubit
                                                      .deleteStudent(cubit
                                                          .students[index]
                                                          .docid!)
                                                      .then((value) {
                                                    cubit.students
                                                        .removeAt(index);
                                                  });
                                                });
                                              },
                                              onEdit: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        UpdateStudent(
                                                      collectionname:
                                                          'students',
                                                      docid: cubit
                                                          .students[index]
                                                          .docid!,
                                                    ),
                                                  ),
                                                );
                                              }, index: index,);
                                        });
                      },
                    ),
                  ]),
                  const SizedBox(height: 30),
                  Column(children: [
                    Container(
                        margin: const EdgeInsets.symmetric(horizontal: 30),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text('الباصات',
                                  style: TextStyle(
                                      fontFamily: 'Urbanist',
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => ViewAllBuses()));
                                  },
                                  child: const Text('عرض الجميع',
                                      style: TextStyle(
                                          fontFamily: 'Urbanist',
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xFF6E234E),
                                          decoration:
                                              TextDecoration.underline)))
                            ])),
                    const SizedBox(height: 10),
                    BlocBuilder<OpBussesCubit, OpStates>(
                      builder: (context, state) {
                        var cubit = OpBussesCubit.get(context);
                        return state is LoadingState
                            ? ShimmerWidgetGrid(
                                count: 2,
                                width: 35.w,
                                height: 20.h,
                                itemcount: 2,
                              )
                            : state is ErrorState
                                ? const SizedBox()
                                : cubit.buses.isEmpty
                                    ? const Center(
                                        child: Text('لا يوجد باصات',
                                            style: TextStyle(
                                                fontSize: 25,
                                                fontFamily: 'Urbanist',
                                                fontWeight: FontWeight.bold)),
                                      )
                                    : GridView.builder(
                                        shrinkWrap: true,
                                        physics: const ScrollPhysics(),
                                        gridDelegate:
                                            const SliverGridDelegateWithFixedCrossAxisCount(
                                                crossAxisSpacing: 10,
                                                mainAxisSpacing: 10,
                                                childAspectRatio: 5 / 4,
                                                crossAxisCount: 2
                                                ),
                                        itemCount: cubit.buses.length,
                                        itemBuilder: (context, index) {
                                          return BusCard(
                                              index: index,
                                              busNumber:
                                                  '${cubit.buses[index].busnumber}',
                                              delete: () {
                                                setState(() {
                                                  cubit
                                                      .deleteBus(cubit
                                                          .buses[index].docid!)
                                                      .then((value) {
                                                    cubit.buses.removeAt(index);
                                                  });
                                                });
                                              },
                                              onEdit: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        UpdateBus(
                                                      collectionname: 'buses',
                                                      docid: cubit
                                                          .buses[index].docid!,
                                                    ),
                                                  ),
                                                );
                                              });
                                        });
                      },
                    ),
                  ]),
                  const SizedBox(height: 30),
                  Column(children: [
                    Container(
                        margin: const EdgeInsets.symmetric(horizontal: 30),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text('السائق',
                                  style: TextStyle(
                                      fontFamily: 'Urbanist',
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                              InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => ViewAllDrivers()));
                                  },
                                  child: const Text('عرض الكل',
                                      style: TextStyle(
                                          fontFamily: 'Urbanist',
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xFF6E234E),
                                          decoration:
                                              TextDecoration.underline)))
                            ])),
                    const SizedBox(height: 10),
                    BlocBuilder<OpDriverCubit, OpStates>(
                      builder: (context, state) {
                        var cubit = OpDriverCubit.get(context);
                        return state is LoadingState
                            ? ShimmerWidgetGrid(
                                count: 2,
                                width: 35.w,
                                height: 20.h,
                                itemcount: 2,
                              )
                            : state is ErrorState
                                ? SizedBox()
                                : cubit.drivers.length == 0
                                    ? const Center(
                                        child: Text('لا يوجد سائقين',
                                            style: TextStyle(
                                                fontSize: 25,
                                                fontFamily: 'Urbanist',
                                                fontWeight: FontWeight.bold)),
                                      )
                                    : GridView.builder(
                                        shrinkWrap: true,
                                        physics: const ScrollPhysics(),
                                        gridDelegate:
                                            const SliverGridDelegateWithFixedCrossAxisCount(
                                                crossAxisSpacing: 10,
                                                mainAxisSpacing: 10,
                                                childAspectRatio: 5 / 4,
                                                crossAxisCount: 2),
                                        itemCount: cubit.drivers.length,
                                        itemBuilder: (context, index) {
                                          return StudentAndDriverCard(
                                            isDriverPage: true,
                                              id: cubit
                                                  .drivers[index].docid!,
                                              studentName: cubit
                                                  .drivers[index].drivername,
                                              delete: () {
                                                setState(() {
                                                  cubit
                                                      .deleteDriver(cubit
                                                          .drivers[index]
                                                          .docid!)
                                                      .then((value) {
                                                    cubit.drivers
                                                        .removeAt(index);
                                                  });
                                                });
                                              },
                                              onEdit: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        UpdateDriver(
                                                      collectionname: 'drivers',
                                                      docid: cubit
                                                          .drivers[index]
                                                          .docid!,
                                                    ),
                                                  ),
                                                );
                                              }, index: index,);
                                        });
                      },
                    ),
                  ]),
                  // const SizedBox(
                  //   height: 90,
                  // )
                ]),
          ),
        )),
      ),
      // homeButton: () {},
    );
  }
}
