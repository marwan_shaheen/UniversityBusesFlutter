import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:student_bus_project/Features/Student/Page/students_list.dart';
import 'package:student_bus_project/Utils/Widget/PageStructure.dart';
import 'package:student_bus_project/Utils/Widget/SnackBar.dart';

import '../../../Utils/Widget/CostumeTextField.dart';
import '../../../Utils/Widget/CostumeTopBackBottum.dart';
import '../../../Utils/Widget/PrimaryBottun.dart';
import '../Cubit/cubit.dart';
import '../Cubit/states.dart';

class AddNewStudent extends StatefulWidget {
  const AddNewStudent({super.key});

  @override
  State<AddNewStudent> createState() => _AddNewStudentState();
}

class _AddNewStudentState extends State<AddNewStudent> {
  @override
  Widget build(BuildContext context) {
    TextEditingController studentNameController = TextEditingController();
    TextEditingController userNameController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    TextEditingController cpasswordController = TextEditingController();
    var formkey = GlobalKey<FormState>();
    return PageStructure(
        body: BlocProvider(
          create: (context) => OpStudentsCubit(),
          child: BlocConsumer<OpStudentsCubit, OpStates>(
            listener: (context, state) {
              if (state is SuccessState) {
                ScaffoldMessenger.of(context).showSnackBar(ShowToastt(
                    text: 'تم التسجيل بنجاح', state: ToastStates.Succes));
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ViewAllStudentPage(),
                  ),
                );
              }
            },
            builder: (context, state) {
              var cubit = OpStudentsCubit.get(context);
              return SingleChildScrollView(
                  child: Form(
                key: formkey,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const CostumeTopBackButton(),
                        const Text('إضافة طالب',
                            style: TextStyle(
                                fontSize: 22,
                                fontFamily: 'Urbanist',
                                fontWeight: FontWeight.bold)),
                        const SizedBox(height: 35),
                        CostumeTextField(
                            validate: (value) {
                              if (value!.isEmpty) {
                                return "حقل الطالب مطلوب";
                              }
                              return null;
                            },
                            controller: studentNameController,
                            hint: 'اسم الطالب'),
                        const SizedBox(height: 20),
                        CostumeTextField(
                          controller: userNameController,
                          hint: 'اسم المستخدم',
                          validate: (value) {
                            if (value!.isEmpty) {
                              return "حقل اسم المستخدم مطلوب";
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 20),
                        CostumeTextField(
                          controller: passwordController,
                          hint: 'كلمة المرور',
                          validate: (value) {
                            if (value!.isEmpty) {
                              return "كلمة المرور مطلوبة";
                            } else if (value!.isEmpty || value.length < 8) {
                              return "يجب أن لا يقل طول كلمة المرور عن 8";
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 20),
                        CostumeTextField(
                          controller: cpasswordController,
                          hint: 'تأكيد كملة المرور',
                          validate: (value) {
                            if (value != passwordController.text)
                              return "كلمات المرور غير متطابقة";
                            return null;
                          },
                        ),
                        const SizedBox(height: 50),
                        ConditionalBuilder(
                          condition: state is! LoadingState,
                          builder: (context) => PrimaryButton(
                              label: 'إضافة',
                              onPressed: () {
                                if (formkey.currentState!.validate()) {
                                  cubit.addStudent({
                                    'id': '',
                                    'password': passwordController.text,
                                    'student_name': studentNameController.text,
                                    'username': userNameController.text,
                                  });
                                }
                              }),
                          fallback: (context) => const Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                        const SizedBox(height: 95)
                      ]),
                ),
              ));
            },
          ),
        ),
        homeButton: () {
          Navigator.pop(context);
          // Navigator.pop(context);
        });
  }
}
