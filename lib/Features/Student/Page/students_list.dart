import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:student_bus_project/Features/Home/Page/home_page.dart';
import 'package:student_bus_project/Features/Student/Page/add_student.dart';
import 'package:student_bus_project/Features/Student/Page/updatestudent.dart';
import 'package:student_bus_project/Utils/Widget/PageStructure.dart';
import 'package:student_bus_project/Utils/Widget/Shimmer.dart';
import 'package:student_bus_project/Utils/Widget/StudentCard.dart';

import '../../../Utils/helper.dart';
import '../Cubit/cubit.dart';
import '../Cubit/states.dart';

class ViewAllStudentPage extends StatefulWidget {
  // const ViewAllStudentPage({Key? key}) : super(key: key);

  @override
  _ViewAllStudentPageState createState() => _ViewAllStudentPageState();
}

class _ViewAllStudentPageState extends State<ViewAllStudentPage> {
  @override
  Widget build(BuildContext context) {
    return PageStructure(
        flatingactionbutton: FloatingActionButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
            heroTag: 'second',
            backgroundColor: Color(0xFFDB946E),
            child: Icon(Icons.add, color: Colors.white),
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => AddNewStudent()));
            }),
        body: BlocProvider(
          create: (context) => OpStudentsCubit()..getStudents(),
          child: BlocConsumer<OpStudentsCubit, OpStates>(
            listener: (context, state) {},
            builder: (context, state) {
              var cubit = OpStudentsCubit.get(context);
              return Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: SingleChildScrollView(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        const SizedBox(height: 30),
                        Row(children: [
                          Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: const Color(0xFFE8ECF4)),
                                borderRadius: BorderRadius.circular(16)),
                            margin:
                                const EdgeInsets.symmetric(vertical: 15),
                            child: IconButton(
                                onPressed: () {
                                  goToHome(context);
                                },
                                color: const Color(0xFF1E232C),
                                icon: const Icon(
                                    Icons.arrow_back_ios_rounded,
                                    size: 20)),
                          ),
                          const Spacer()
                        ]),
                        const SizedBox(height: 25),
                        const Text('الطلاب',
                            style: TextStyle(
                                fontSize: 22,
                                fontFamily: 'Urbanist',
                                fontWeight: FontWeight.bold)),
                        state is LoadingState
                            ? ShimmerWidgetGrid(
                                count: 2,
                                width: 35.w,
                                height: 80.h,
                                itemcount: 4,
                              )
                            : state is ErrorState
                                ? SizedBox()
                                : cubit.students.length == 0
                                    ? const Center(
                                        child: Text('لا يوجد طلاب',
                                            style: TextStyle(
                                                fontSize: 25,
                                                fontFamily: 'Urbanist',
                                                fontWeight:
                                                    FontWeight.bold)),
                                      )
                                    : GridView.builder(
                                        shrinkWrap: true,
                                        physics: const ScrollPhysics(),
                                        gridDelegate:
                                            const SliverGridDelegateWithFixedCrossAxisCount(
                                                crossAxisSpacing: 10,
                                                mainAxisSpacing: 10,
                                                crossAxisCount: 2),
                                        itemCount: cubit.students.length,
                                        itemBuilder: (context, index) {
                                          return StudentAndDriverCard(
                                            id: cubit.students[index].docid!,
                                            index: index,
                                              studentName:
                                                  '${cubit.students[index].studentname}',
                                              delete: () {
                                                setState(() {
                                                  cubit
                                                      .deleteStudent(cubit
                                                          .students[index]
                                                          .docid!)
                                                      .then((value) {
                                                    cubit.students
                                                        .removeAt(index);
                                                  });
                                                });
                                              },
                                              onEdit: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        UpdateStudent(
                                                      collectionname:
                                                          'students',
                                                      docid: cubit
                                                          .students[index]
                                                          .docid!,
                                                    ),
                                                  ),
                                                );
                                              });
                                        }),
                        const SizedBox(height: 90)
                      ])));
            },
          ),
        ),
        homeButton: () {
          goToHome(context);
        });
  }
}
