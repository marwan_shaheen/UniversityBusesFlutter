import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:student_bus_project/Features/Home/Page/home_page.dart';
import 'package:student_bus_project/Features/Login/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Login/Page/login.dart';
import 'package:student_bus_project/Features/Student/Cubit/states.dart';
import 'package:student_bus_project/Features/Student/Page/students_list.dart';
import 'package:student_bus_project/Utils/Widget/CostumeTextField.dart';
import 'package:student_bus_project/Utils/Widget/CostumeTopBackBottum.dart';
import 'package:student_bus_project/Utils/Widget/PageStructure.dart';
import 'package:student_bus_project/Utils/Widget/PrimaryBottun.dart';
import 'package:student_bus_project/Utils/Widget/SnackBar.dart';
import 'package:student_bus_project/remote/Cach_Helper.dart';

import '../../../Utils/helper.dart';

class UpdateStudent extends StatelessWidget {
  var collectionname, docid;
  UpdateStudent({collectionname, docid}) {
    this.collectionname = collectionname;
    this.docid = docid;
  }
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController userNameController = TextEditingController();
    TextEditingController passwordController = TextEditingController();
    TextEditingController cpasswordController = TextEditingController();
    var formkey = GlobalKey<FormState>();
    return PageStructure(
        body: BlocProvider(
          create: (context) => AuthCubit()
            ..getProfile(collection: collectionname, id: docid),
          child: BlocConsumer<AuthCubit, OpStates>(
            listener: (context, state) {
              if (state is SuccessUpdateState) {
                ScaffoldMessenger.of(context).showSnackBar(ShowToastt(
                    text: 'تم تعديل المعلومات بنجاح', state: ToastStates.Succes));
                goToHome(context);
              }
            },
            builder: (context, state) {
              var cubit = AuthCubit.get(context);
              if (cubit.student != null) {
                nameController.text = cubit.student!.studentname.toString();
                passwordController.text = cubit.student!.password.toString();
                userNameController.text = cubit.student!.username.toString();
              }
              return SingleChildScrollView(
                  child: Form(
                key: formkey,
                child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          const CostumeTopBackButton(),
                          const Text('تعديل المعلومات',
                              style: TextStyle(
                                  fontSize: 22,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.bold)),
                          const SizedBox(height: 35),
                          CostumeTextField(
                              validate: (value) {
                                if (value!.isEmpty) {
                                  return "حقل اسم الطالب مطلوب";
                                }
                                return null;
                              },
                              controller: nameController,
                              hint: 'اسم الطالب'),
                          const SizedBox(height: 20),
                          CostumeTextField(
                            controller: userNameController,
                            hint: 'اسم المستخدم',
                            validate: (value) {
                              if (value!.isEmpty) {
                                return "حقل اسم المستخدم مطلوب";
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 20),
                          CostumeTextField(
                            controller: passwordController,
                            hint: 'كلمة المرور',
                            validate: (value) {
                              if (value!.isEmpty) {
                                return "كلمة المرور مطلوبة";
                              } else if (value.isEmpty || value.length < 8) {
                                return "يجب أن لا يقل طول كلمة المرور عن 8";
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 20),
                          CostumeTextField(
                            controller: cpasswordController,
                            hint: 'تأكيد كلمة المرور',
                            validate: (value) {
                              if (value != passwordController.text) {
                                return "كلمات المرور غير متطابقة";
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 50),
                          ConditionalBuilder(
                            condition: state is! LoadingState,
                            builder: (context) => PrimaryButton(
                                label: 'حفظ',
                                onPressed: () {
                                  if (formkey.currentState!.validate()) {
                                    cubit.updateStudent(
                                        collection: collectionname,
                                        id: docid,
                                        data: {
                                          'id': '',
                                          'password': passwordController.text,
                                          'student_name': nameController.text,
                                          'username': userNameController.text,
                                        });
                                  }
                                }),
                            fallback: (context) => const Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                          const SizedBox(height: 95)
                        ])),
              ));
            },
          ),
        ),
        homeButton: () {
          Navigator.pop(context);
          // Navigator.pop(context);
        });
  }
}
