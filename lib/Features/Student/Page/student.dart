
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


import 'package:student_bus_project/drwer.dart';

import '../../Tracking/Cubit/cubit.dart';
import '../../Tracking/tracking_google_map.dart';
import '../Cubit/states.dart';

class StudentPage extends StatefulWidget {
  @override
  State<StudentPage> createState() => _StudentPageState();
  const StudentPage({super.key});
}

class _StudentPageState extends State<StudentPage> {
  final globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        key: globalKey,
        endDrawer: MyDrawer(scaffoldKey: globalKey, collection: 'students'),
        appBar: AppBar(
          title: const Text(
            'صفحة الطالب',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0,
          actions: [
            IconButton(
              onPressed: () {
                globalKey.currentState!.openEndDrawer();
              },
              icon: const Icon(
                Icons.person_2_sharp,
                size: 30,
                color: Colors.black,
              ),
            ),
          ],
        ),
        body: const TrackingGoogleMap(trackType: TrackType.tracker,));
  }
}
