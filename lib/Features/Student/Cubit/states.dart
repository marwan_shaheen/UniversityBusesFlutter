import '../../../Models/Buses.dart';

abstract class OpStates {}

class OpInitialState extends OpStates {}

class LoadingState extends OpStates {}

class SuccessState extends OpStates {}

class NotFoundState extends OpStates {}

class SuccessUpdateState extends OpStates {}

class GetLatLongStateSuccess extends OpStates {}
class GetTrackedBusSuccess extends OpStates{
  Bus bus;
  GetTrackedBusSuccess({required this.bus});
}
class GetTrackedBusLoading extends OpStates{}
class GetTrackedBusError extends OpStates{}

class ErrorState extends OpStates {
  final String error;
  ErrorState(this.error);
}
