import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:student_bus_project/Core/firestore_helper.dart';
import 'package:student_bus_project/Features/Student/Cubit/states.dart';
import 'package:student_bus_project/Models/Student.dart';

class OpStudentsCubit extends Cubit<OpStates> {
  OpStudentsCubit() : super(OpInitialState());
  static OpStudentsCubit get(context) => BlocProvider.of(context);

  Future<void> addStudent(Map<String, dynamic> student) async {
    try {
      emit(LoadingState());
      await FireStoreHelper.postDocument('students', student);
      emit(SuccessState());
    } catch (error) {
      emit(ErrorState(error.toString()));
    }
  }

  List<Student> students = [];
  Student? student;
  Future<void> getStudents() async {
    try {
      emit(LoadingState());
      final response = await FireStoreHelper.getDocuments('students');
      for (var element in response.docs) {

      var student = Student.fromjson(element.data());
      student.docid = element.id; 
      students.add(student);
    }
      emit(SuccessState());
    } catch (error) {
      emit(ErrorState(error.toString()));
    }
  }

  void getStudentsDetails(String id)async{
    try {
      emit(LoadingState());

      final response = await FireStoreHelper.getInformationInDocument(path:'students',documentId: id);
      student = Student.fromjson(response.data()!);

      emit(SuccessState());
    } catch (error) {
      emit(ErrorState(error.toString()));
    }
  }

   Future<void> deleteStudent(String documentId) async {
    try {
      emit(LoadingState());
      await FireStoreHelper.deleteDocument('students', documentId);
      emit(SuccessState());
    } catch (error) {
      emit(ErrorState(error.toString()));
    }
  }
   
}
