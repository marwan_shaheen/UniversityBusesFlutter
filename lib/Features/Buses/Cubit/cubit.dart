
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:student_bus_project/Core/firestore_helper.dart';
import 'package:student_bus_project/Models/Buses.dart';

import '../../Student/Cubit/states.dart';

class OpBussesCubit extends Cubit<OpStates> {
  OpBussesCubit() : super(OpInitialState());
  static OpBussesCubit get(context) => BlocProvider.of(context);
  Bus? bus;
  Future<void> addBus(Map<String, dynamic> bus) async {
    try {
      emit(LoadingState());
      await FireStoreHelper.postDocument('buses', bus);
      emit(SuccessState());
    } catch (error) {
      emit(ErrorState(error.toString()));
    }
  }

  List<Bus> buses = [];
  Future<void> getBusesEvent({Map<String,dynamic>? filter}) async {
    try {
      emit(LoadingState());

      buses = await getBuses(filter: filter);
      emit(SuccessState());
    } catch (error) {
      emit(ErrorState(error.toString()));
    }
  }


  Future<List<Bus>> getBuses({Map<String,dynamic>? filter}) async {

      final response = await FireStoreHelper.getFilteredDocuments('buses',filter);
      List<Bus> buses2 =[];
      for (var element in response.docs) {
        var bus = Bus.fromjson(element.data());
        bus.docid = element.id;
        buses2.add(bus);
      }
      return buses2;

  }



 Future<void> getOneBusEvent(String id)async{
    try{
      emit(LoadingState());
      bus = await getOneBus(id);
      if(bus == null){
        emit(ErrorState('no buses'));
      }else {
        emit(SuccessState());
      }
    }catch(error){
      emit(ErrorState(error.toString()));
    }
  }

  Future<Bus?> getOneBus(String id)async{
    final result = await FireStoreHelper.getInformationInDocument(path:'buses',documentId: id);
    if(result.exists && result.data()!= null){
      return Bus.fromjson(result.data()!);
    }else{
      return  null;
    }
  }

  Future<void> deleteBus(String documentId) async {
    try {
      emit(LoadingState());
      await FirebaseDatabase.instance.ref('buses').child(documentId).remove();
      await FireStoreHelper.deleteDocument('buses', documentId);
      emit(SuccessState());
    } catch (error) {
      print('hello error $error');
      emit(ErrorState(error.toString()));
    }
  }
}
