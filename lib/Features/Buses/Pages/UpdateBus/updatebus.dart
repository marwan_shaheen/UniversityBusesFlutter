import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:student_bus_project/Features/Driver/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Home/Page/home_page.dart';
import 'package:student_bus_project/Features/Login/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Login/Page/login.dart';
import 'package:student_bus_project/Features/Student/Cubit/states.dart';
import 'package:student_bus_project/Features/Student/Page/students_list.dart';
import 'package:student_bus_project/Models/Driver.dart';
import 'package:student_bus_project/Utils/Widget/CostumeTextField.dart';
import 'package:student_bus_project/Utils/Widget/CostumeTopBackBottum.dart';
import 'package:student_bus_project/Utils/Widget/PageStructure.dart';
import 'package:student_bus_project/Utils/Widget/PrimaryBottun.dart';
import 'package:student_bus_project/Utils/Widget/SnackBar.dart';
import 'package:student_bus_project/Utils/helper.dart';

class UpdateBus extends StatefulWidget {
  var collectionname, docid;
  UpdateBus({collectionname, docid}) {
    this.collectionname = collectionname;
    this.docid = docid;
  }

  @override
  State<UpdateBus> createState() => _UpdateBusState();
}

class _UpdateBusState extends State<UpdateBus> {
  TextEditingController busNumberController = TextEditingController();
  TextEditingController numberOfSeatsController = TextEditingController();
  TextEditingController stopStationController = TextEditingController();
  TextEditingController goOffTimeController = TextEditingController();
  TextEditingController arriveTimeController = TextEditingController();
  var formkey = GlobalKey<FormState>();
  Driver? selectedValue;
  @override
  Widget build(BuildContext context) {
    return PageStructure(
        body: BlocProvider(
          create: (context) => AuthCubit()
            ..getProfile(
                collection: widget.collectionname,
                id: widget.docid),
          child: BlocConsumer<AuthCubit, OpStates>(
            listener: (context, state) {
              if (state is SuccessUpdateState) {
                ScaffoldMessenger.of(context).showSnackBar(ShowToastt(
                    text: 'تم التحديث بنجاح', state: ToastStates.Succes));
                goToHome(context);
              }
            },
            builder: (context, state) {
              var cubit = AuthCubit.get(context);
              if (cubit.buses != null) {
                busNumberController.text = cubit.buses!.busnumber.toString();
                numberOfSeatsController.text =
                    cubit.buses!.numbofseats.toString();
                stopStationController.text =
                    cubit.buses!.stopstations.toString();
                goOffTimeController.text = cubit.buses!.goofftime.toString();
                arriveTimeController.text = cubit.buses!.arrivetiome.toString();
              }
              return SingleChildScrollView(
                  child: Form(
                key: formkey,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        const CostumeTopBackButton(),
                        const Text('تحديث المعلومات',
                            style: TextStyle(
                                fontSize: 22,
                                fontFamily: 'Urbanist',
                                fontWeight: FontWeight.bold)),
                        const SizedBox(height: 35),
                        if (widget.collectionname == 'buses')
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                CostumeTextField(
                                  controller: busNumberController,
                                  hint: 'رقم الباص',
                                  validate: (value) {
                                    if (value!.isEmpty) {
                                      return "رقم الباص مطلوب";
                                    }
                                    return null;
                                  },
                                ),
                                SizedBox(height: 20),
                                CostumeTextField(
                                  controller: numberOfSeatsController,
                                  hint: 'عدد المقاعد',
                                  validate: (value) {
                                    if (value!.isEmpty) {
                                      return "عدد المقاعد مطلوب";
                                    }
                                    return null;
                                  },
                                ),
                                SizedBox(height: 20),
                                CostumeTextField(
                                  controller: stopStationController,
                                  hint: 'نقاط التوقف',
                                  validate: (value) {
                                    if (value!.isEmpty) {
                                      return "نقاط التوقف مطلوبة";
                                    }
                                    return null;
                                  },
                                ),
                                const SizedBox(height: 20),
                                CostumeTextField(
                                  controller: goOffTimeController,
                                  hint: 'وقت الانطلاق',
                                  validate: (value) {
                                    if (value!.isEmpty) {
                                      return "وقت الانطلاق مطلوب";
                                    }
                                    return null;
                                  },
                                ),
                                SizedBox(height: 20),
                                CostumeTextField(
                                  controller: arriveTimeController,
                                  hint: 'وقت الوصول',
                                  validate: (value) {
                                    if (value!.isEmpty) {
                                      return "وقت الوصول مطلوب";
                                    }
                                    return null;
                                  },
                                ),
                                const SizedBox(height: 20),
                                BlocBuilder<OpDriverCubit, OpStates>(
                                    builder: (context, state) {
                                  var cubit = OpDriverCubit.get(context);
                                  if (state is SuccessState) {
                                    //selectedValue=
                                    return cubit.drivers.isNotEmpty
                                        ? Container(
                                            height: 8.h,
                                            decoration: ShapeDecoration(
                                                color: const Color(0xFFF7F8F9),
                                                shape: RoundedRectangleBorder(
                                                    side: const BorderSide(
                                                        color:
                                                            Color(0xFFE8ECF4)),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            12))),
                                            padding: EdgeInsets.symmetric(
                                              horizontal: 2.w,
                                            ),
                                            width: double.infinity,
                                            child:
                                                DropdownButtonFormField<Driver>(
                                              isExpanded: true,
                                              // underline: Container(),
                                              validator: (value) {
                                                if (selectedValue == null) {
                                                  return "السائق مطلوب";
                                                }
                                                return null;
                                              },
                                              hint: const Text(
                                                  "اختر السائق",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      color: Color(0xFF8391A1),
                                                      fontSize: 13)),
                                              value: selectedValue,
                                              items: cubit.drivers.map((e) {
                                                return DropdownMenuItem<Driver>(
                                                  value: e,
                                                  child: Text(e.drivername!),
                                                );
                                              }).toList(),
                                              onChanged: (value) {
                                                setState(() {
                                                  selectedValue = value;
                                                });
                                              },
                                            ))
                                        : const Text("Error in dropdown");
                                  } else {
                                    return const Center(
                                        child: SizedBox(
                                            height: 50,
                                            width: 50,
                                            child:
                                                CircularProgressIndicator()));
                                  }
                                }),
                              ]),
                        const SizedBox(height: 50),
                        ConditionalBuilder(
                          condition: state is! LoadingState,
                          builder: (context) => PrimaryButton(
                              label: 'حفظ التعديلات',
                              onPressed: () {
                                if (formkey.currentState!.validate()) {
                                  cubit.updateStudent(
                                      collection: widget.collectionname,
                                      id: widget.docid,
                                      data: {
                                        'id': '',
                                        'bus_number': busNumberController.text,
                                        'num_of_seats':
                                            numberOfSeatsController.text,
                                        'stop_stations':
                                            stopStationController.text,
                                        'go_off_time': goOffTimeController.text,
                                        'arrive_time':
                                            arriveTimeController.text,
                                        'driver_name':
                                            selectedValue!.drivername,
                                      });
                                }
                              }),
                          fallback: (context) => const Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                        const SizedBox(height: 95)
                      ]),
                ),
              ));
            },
          ),
        ),
        homeButton: () {
          Navigator.pop(context);
          // Navigator.pop(context);
        });
  }
}
