import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:student_bus_project/Features/Buses/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Driver/Cubit/cubit.dart';

import '../../Student/Cubit/states.dart';

class BusesDetails extends StatelessWidget {
  final int index;
  const BusesDetails(this.index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => OpBussesCubit()..getBusesEvent()),
        ],
        child: BlocConsumer<OpBussesCubit, OpStates>(
          listener: (context, state) {},
          builder: (context, state) {
            var cubit = OpBussesCubit.get(context);
            return SafeArea(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 30),
                        Row(children: [
                          Container(
                            decoration: BoxDecoration(
                                border:
                                    Border.all(color: const Color(0xFFE8ECF4)),
                                borderRadius: BorderRadius.circular(16)),
                            margin: const EdgeInsets.symmetric(vertical: 15),
                            child: IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                color: const Color(0xFF1E232C),
                                icon: const Icon(Icons.arrow_back_ios_rounded,
                                    size: 20)),
                          ),
                          const Spacer()
                        ]),
                        const SizedBox(height: 25),
                        state is LoadingState
                            ? const Center(child: CircularProgressIndicator())
                            : state is ErrorState
                                ? const SizedBox()
                                : Container(
                                    margin: const EdgeInsets.symmetric(
                                      horizontal: 5,
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 25),
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(18),
                                      color: const Color(0xFFE8ECF4),
                                    ),
                                    child: Wrap(
                                      runSpacing: 30,

                                      alignment: WrapAlignment.spaceBetween,
                                      // crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        RichText(
                                          text: TextSpan(children: [
                                            const TextSpan(
                                              text: "رقم الباص: ",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            TextSpan(
                                              text:
                                                  cubit.buses[index].busnumber!,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ]),
                                        ),
                                        RichText(
                                          text: TextSpan(children: [
                                            const TextSpan(
                                              text: "عدد المقاعد: ",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            TextSpan(
                                              text: cubit
                                                  .buses[index].numbofseats!,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ]),
                                        ),
                                        RichText(
                                          text: TextSpan(children: [
                                            const TextSpan(
                                              text: "نقاط التوقف: ",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            TextSpan(
                                              text: cubit
                                                  .buses[index].stopstations!,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ]),
                                        ),
                                        RichText(
                                          text: TextSpan(children: [
                                            const TextSpan(
                                              text: "وقت الانطلاق: ",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            TextSpan(
                                              text:
                                                  cubit.buses[index].goofftime!,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ]),
                                        ),
                                        RichText(
                                          text: TextSpan(children: [
                                            const TextSpan(
                                              text: "وقت الوصول: ",
                                              style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            TextSpan(
                                              text: cubit
                                                  .buses[index].arrivetiome!,
                                              style: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ]),
                                        ),
                                        // BlocBuilder<OpDriverCubit, OpStates>(
                                        //   builder: (context, state) {
                                        //     var cubit =
                                        //         OpDriverCubit.get(context);
                                        //     return cubit.drivers.isNotEmpty
                                        //         ? RichText(
                                        //             text: TextSpan(children: [
                                        //               const TextSpan(
                                        //                 text: "Driver Name : ",
                                        //                 style: TextStyle(
                                        //                     color: Colors.green,
                                        //                     fontSize: 18,
                                        //                     fontFamily:
                                        //                         'Urbanist',
                                        //                     fontWeight:
                                        //                         FontWeight
                                        //                             .bold),
                                        //               ),
                                        //               TextSpan(
                                        //                 text: cubit
                                        //                     .drivers[index]
                                        //                     .drivername!
                                        //                     .toString(),
                                        //                 style: TextStyle(
                                        //                     color: Colors.black,
                                        //                     fontSize: 18,
                                        //                     fontFamily:
                                        //                         'Urbanist',
                                        //                     fontWeight:
                                        //                         FontWeight
                                        //                             .bold),
                                        //               ),
                                        //             ]),
                                        //           )
                                        //         : Center(
                                        //             child:
                                        //                 CircularProgressIndicator(),
                                        //           );
                                        //   },
                                        // )
                                      ],
                                    ),
                                  ),
                        // const SizedBox(
                        //   height: 20,
                        // ),
                        // SizedBox(
                        //     height: 400,
                        //     child: ListView.builder(
                        //       itemCount: 5,
                        //       itemBuilder: (context, index) {
                        //         return Padding(
                        //           padding:
                        //               const EdgeInsets.symmetric(vertical: 10),
                        //           child: Text(
                        //             "Student ${index + 1}",
                        //             style: const TextStyle(fontSize: 20),
                        //           ),
                        //         );
                        //       },
                        //     ))
                      ]),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
