import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:student_bus_project/Features/Buses/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Driver/Cubit/cubit.dart';
import 'package:student_bus_project/Models/Driver.dart';
import 'package:student_bus_project/Utils/Widget/SnackBar.dart';

import '../../../Utils/Widget/CostumeTextField.dart';
import '../../../Utils/Widget/CostumeTopBackBottum.dart';
import '../../../Utils/Widget/PageStructure.dart';
import '../../../Utils/Widget/PrimaryBottun.dart';
import '../../Student/Cubit/states.dart';
import 'buses.dart';

class AddNewBuses extends StatefulWidget {
  const AddNewBuses({super.key});

  @override
  State<AddNewBuses> createState() => _AddNewBusesState();
}

class _AddNewBusesState extends State<AddNewBuses> {
  TextEditingController busNumberController = TextEditingController();
  TextEditingController numberOfSeatsController = TextEditingController();
  TextEditingController stopStationController = TextEditingController();
  TextEditingController goOffTimeController = TextEditingController();
  TextEditingController arriveTimeController = TextEditingController();
  var formkey = GlobalKey<FormState>();
  Driver? selectedValue;
  @override
  Widget build(BuildContext context) {
    return PageStructure(
        body: MultiBlocProvider(
          providers: [
            BlocProvider(create: (_) => OpDriverCubit()..getDrivers()),
          ],
          child: BlocConsumer<OpBussesCubit, OpStates>(
            listener: (context, state) {
              if (state is SuccessState) {
                ScaffoldMessenger.of(context).showSnackBar(ShowToastt(
                    text: "تمت الإضافة بنجاح", state: ToastStates.Succes));
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ViewAllBuses(),
                  ),
                );
              }
            },
            builder: (context, state) {
              var cubit = OpBussesCubit.get(context);
              return Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: SingleChildScrollView(
                      child: Form(
                    key: formkey,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          const CostumeTopBackButton(),
                          const Text('إضافة',
                              style: TextStyle(
                                  fontSize: 22,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.bold)),
                          const SizedBox(height: 35),
                          CostumeTextField(
                            controller: busNumberController,
                            hint: 'رقم الباص',
                            validate: (value) 
                            {
                              if (value!.isEmpty) {
                                return "رقم الباص مطلوب";
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 20),
                          CostumeTextField(
                            controller: numberOfSeatsController,
                            hint: 'عدد المقاعد',
                            validate: (value) {
                              if (value!.isEmpty) {
                                return "عدد المقاعد مطلوب";
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 20),
                          CostumeTextField(
                            controller: stopStationController,
                            hint: 'نقاط التوقف',
                            validate: (value) {
                              if (value!.isEmpty) {
                                return "نقاط التوقف مطلوبة";
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 20),
                          CostumeTextField(
                            controller: goOffTimeController,
                            hint: 'وقت الانطلاق',
                            validate: (value) {
                              if (value!.isEmpty) {
                                return "وقت الانظلاق مطلوب";
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 20),
                          CostumeTextField(
                            controller: arriveTimeController,
                            hint: 'وقت الوصول',
                            validate: (value) {
                              if (value!.isEmpty) {
                                return "وقت الوصول مطلوب";
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 20),
                          BlocBuilder<OpDriverCubit, OpStates>(
                              builder: (context, state) {
                            var cubit = OpDriverCubit.get(context);
                            if (state is SuccessState) {
                              return cubit.drivers.isNotEmpty
                                  ? Container(
                                      height: 8.h,
                                      decoration: ShapeDecoration(
                                          color: const Color(0xFFF7F8F9),
                                          shape: RoundedRectangleBorder(
                                              side: const BorderSide(
                                                  color: Color(0xFFE8ECF4)),
                                              borderRadius:
                                                  BorderRadius.circular(12))),
                                      padding: EdgeInsets.symmetric(
                                        horizontal: 2.w,
                                      ),
                                      width: double.infinity,
                                      child: DropdownButtonFormField<Driver>(
                                        isExpanded: true,
                                        validator: (value) {
                                          if (selectedValue == null) {
                                            return "يجب اختيار السائق";
                                          }
                                          return null;
                                        },
                                        hint: const Text("اختر السائق",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                color: Color(0xFF8391A1),
                                                fontSize: 13)),
                                        value: selectedValue,
                                        items: cubit.drivers.map((e) {
                                          return DropdownMenuItem<Driver>(
                                            value: e,
                                            child: Text(e.drivername!),
                                          );
                                        }).toList(),
                                        onChanged: (value) {
                                          setState(() {
                                            selectedValue = value;
                                          });
                                        },
                                      ))
                                  : const Text("Error in dropdown");
                            } else {
                              return const Center(
                                  child: SizedBox(
                                      height: 50,
                                      width: 50,
                                      child: CircularProgressIndicator()));
                            }
                          }),
                          const SizedBox(height: 50),
                          ConditionalBuilder(
                            condition: state is! LoadingState,
                            builder: (context) => PrimaryButton(
                                label: 'Add',
                                onPressed: () {
                                  if (formkey.currentState!.validate()) {
                                    cubit.addBus({
                                      'id': '',
                                      'bus_number': busNumberController.text,
                                      'num_of_seats':
                                          numberOfSeatsController.text,
                                      'stop_stations':
                                          stopStationController.text,
                                      'go_off_time': goOffTimeController.text,
                                      'arrive_time': arriveTimeController.text,
                                      'driver_id': selectedValue!.docid,
                                    });
                                  }
                                }),
                            fallback: (context) => const Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                          const SizedBox(height: 95)
                        ]),
                  )));
            },
          ),
        ),
        homeButton: () {
          Navigator.pop(context);
          Navigator.pop(context);
        });
  }
}
