import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';
import 'package:student_bus_project/Features/Home/Page/home_page.dart';
import 'package:student_bus_project/Features/Buses/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Buses/Pages/UpdateBus/updatebus.dart';
import 'package:student_bus_project/Utils/Widget/BusCard.dart';
import 'package:student_bus_project/Utils/Widget/PageStructure.dart';
import 'package:student_bus_project/Utils/Widget/Shimmer.dart';

import '../../../Utils/helper.dart';
import '../../Student/Cubit/states.dart';
import 'add_bus.dart';

class ViewAllBuses extends StatefulWidget {
  const ViewAllBuses({super.key});

  @override
  State<ViewAllBuses> createState() => _ViewAllBusesState();
}

class _ViewAllBusesState extends State<ViewAllBuses> {
  @override
  Widget build(BuildContext context) {
    return PageStructure(
        flatingactionbutton: Positioned(
            bottom: 100,
            right: 20,
            child: FloatingActionButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
                heroTag: 'second',
                backgroundColor: Color(0xFFDB946E),
                child: Icon(Icons.add, color: Colors.white),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (_) => const AddNewBuses()));
                })),
        body: BlocProvider(
          create: (context) => OpBussesCubit()..getBusesEvent(),
          child: BlocConsumer<OpBussesCubit, OpStates>(
            listener: (context, state) {
              // TODO: implement listener
            },
            builder: (context, state) {
              var cubit = OpBussesCubit.get(context);
              return Stack(
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 30),
                          Row(children: [
                            Container(
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: const Color(0xFFE8ECF4)),
                                  borderRadius: BorderRadius.circular(16)),
                              margin: const EdgeInsets.symmetric(vertical: 15),
                              child: IconButton(
                                  onPressed: () {
                                    goToHome(context);
                                  },
                                  color: const Color(0xFF1E232C),
                                  icon: const Icon(Icons.arrow_back_ios_rounded,
                                      size: 20)),
                            ),
                            const Spacer()
                          ]),
                          const SizedBox(height: 25),
                          const Text('الباصات',
                              style: TextStyle(
                                  fontSize: 22,
                                  fontFamily: 'Urbanist',
                                  fontWeight: FontWeight.bold)),
                          state is LoadingState
                              ? ShimmerWidgetGrid(
                                  count: 2,
                                  width: 35.w,
                                  height: 80.h,
                                  itemcount: 2,
                                )
                              : state is ErrorState
                                  ? const SizedBox()
                                  : cubit.buses.isEmpty
                                      ? const Center(
                                          child: Text('لا يوجد باصات',
                                              style: TextStyle(
                                                  fontSize: 25,
                                                  fontFamily: 'Urbanist',
                                                  fontWeight: FontWeight.bold)),
                                        )
                                      : GridView.builder(
                                          shrinkWrap: true,
                                          physics: const ScrollPhysics(),
                                          gridDelegate:
                                              const SliverGridDelegateWithFixedCrossAxisCount(
                                                  crossAxisSpacing: 10,
                                                  mainAxisSpacing: 10,
                                                  childAspectRatio: 5 / 4,
                                                  crossAxisCount: 2),
                                          itemCount: cubit.buses.length,
                                          itemBuilder: (context, index) {
                                            return BusCard(
                                                index: index,
                                                busNumber:
                                                    '${cubit.buses[index].busnumber}',

                                                onEdit: () {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          UpdateBus(
                                                        collectionname: 'buses',
                                                        docid: cubit
                                                            .buses[index]
                                                            .docid!,
                                                      ),
                                                    ),
                                                  );
                                                },
                                              delete: () {  },);
                                          }),
                          const SizedBox(height: 90)
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
        homeButton: () {
          goToHome(context);
        });
  }
}
