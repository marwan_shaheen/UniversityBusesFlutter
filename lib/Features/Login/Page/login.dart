import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:student_bus_project/Features/Driver/Page/driver.dart';
import 'package:student_bus_project/Features/Login/Cubit/cubit.dart';
import 'package:student_bus_project/Features/Home/Page/home_page.dart';
import 'package:student_bus_project/Features/Student/Page/student.dart';
import 'package:student_bus_project/Utils/Widget/CostumeTextField.dart';
import 'package:student_bus_project/Utils/Widget/PrimaryBottun.dart';
import 'package:student_bus_project/Utils/Widget/SnackBar.dart';

import '../../Student/Cubit/states.dart';

class LogInPage extends StatefulWidget {
  final String role;
  const LogInPage(this.role);
  @override
  _LogInPageState createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  TextEditingController userNameController = TextEditingController();
  TextEditingController fullNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmController = TextEditingController();
  var formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocProvider(
      create: (context) => AuthCubit(),
      child: BlocConsumer<AuthCubit, OpStates>(
        listener: (context, state) {
          if (state is SuccessState) {
            ScaffoldMessenger.of(context).showSnackBar(ShowToastt(
                text: 'تم التسجيل بنجاح', state: ToastStates.Succes));
            Navigator.pushAndRemoveUntil(context,
              MaterialPageRoute(
                builder: (context) => widget.role == 'admin'
                    ? HomePage()
                    : widget.role == "students"
                        ? const StudentPage()
                        : DriverPage(),
              ),
              (route) => false,
            );
          } else if (state is NotFoundState) {
            ScaffoldMessenger.of(context).showSnackBar(ShowToastt(
                text: widget.role == 'students'
                    ? 'the Student is Not Found'
                    : widget.role == "admin"
                        ? 'Admin Not Found'
                        : "the Driver is Not Found",
                state: ToastStates.Error));
          }
        },
        builder: (context, state) {
          var cubit = AuthCubit.get(context);
          return SingleChildScrollView(
            child: Form(
              key: formkey,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              border:
                                  Border.all(color: const Color(0xFFE8ECF4)),
                              borderRadius: BorderRadius.circular(16)),
                          margin: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          child: IconButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              color: const Color(0xFF1E232C),
                              icon: const Icon(Icons.arrow_back_ios_rounded,
                                  size: 20)),
                        ),
                        const Spacer()
                      ],
                    ),
                    const SizedBox(height: 20),
                    Container(
                      width: MediaQuery.of(context).size.width * 2 / 3,
                      margin: const EdgeInsets.symmetric(horizontal: 20),
                      child: const Text('مرحبا! سجل دخول للبدء',
                          maxLines: 2,
                          style: TextStyle(
                              fontSize: 26,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Urbanist')),
                    ),
                    const SizedBox(height: 35),
                    Container(
                        margin: const EdgeInsets.symmetric(horizontal: 20),
                        child: CostumeTextField(
                          controller: userNameController,
                          hint: 'اسم المستخدم',
                          validate: (value) {
                            if (value!.isEmpty) {
                              return "اسم المستخدم مطلوب";
                            }
                            return null;
                          },
                        )),
                    const SizedBox(height: 20),
                    // Container(
                    //     margin: const EdgeInsets.symmetric(horizontal: 20),
                    //     child: CostumeTextField(
                    //         controller: fullNameController, hint: 'Full name')),
                    // const SizedBox(height: 20),
                    Container(
                        margin: const EdgeInsets.symmetric(horizontal: 20),
                        child: CostumeTextField(
                          controller: passwordController,
                          hint: 'كلمة المرور ',
                          validate: (value) {
                            if (value!.isEmpty) {
                              return " كلمة المرور مطلوبة";
                            }
                            return null;
                          },
                        )),
                    // const SizedBox(height: 20),
                    // Container(
                    //     margin: const EdgeInsets.symmetric(horizontal: 20),
                    //     child: CostumeTextField(
                    //         controller: confirmController, hint: 'Confirm password')),
                    const SizedBox(height: 35),
                    ConditionalBuilder(
                      condition: state is! LoadingState,
                      builder: (context) => Container(
                          margin: const EdgeInsets.symmetric(horizontal: 20),
                          child: PrimaryButton(
                              label: 'تسجيل الدخول',
                              onPressed: () {
                                if (formkey.currentState!.validate()) {
                                  cubit.login(
                                      widget.role,
                                      widget.role == 'students'
                                          ? 'student_name'
                                          : widget.role == 'admin'
                                              ? 'username'
                                              : 'driver_name',
                                      userNameController.text,
                                      'password',
                                      passwordController.text);
                                }
                              })),
                      fallback: (context) => const Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ]),
            ),
          );
        },
      ),
    ));
  }
}
