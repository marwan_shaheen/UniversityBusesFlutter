import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:student_bus_project/Core/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:student_bus_project/Core/firestore_helper.dart';
import 'package:student_bus_project/Features/Login/Cubit/auth_state.dart';
import 'package:student_bus_project/Models/Buses.dart';
import 'package:student_bus_project/Models/Driver.dart';
import 'package:student_bus_project/Models/Student.dart';
import 'package:student_bus_project/remote/Cach_Helper.dart';

import '../../../Models/Admin.dart';
import '../../Student/Cubit/states.dart';

class AuthCubit extends Cubit<OpStates> {
  AuthCubit() : super(OpInitialState());
  static AuthCubit get(context) => BlocProvider.of(context);
  Future<void> login(String path, String field1, String username, String field2,
      String password) async {
    try {
      emit(LoadingState());
      var searchtodocument = await FireStoreHelper.searchDocuments(
          path, field1, username, field2, password);

      if (searchtodocument.docs.isNotEmpty) {
        saveLogin(password, path, username, searchtodocument.docs.first.id);

        emit(SuccessState());
      } else {
        emit(NotFoundState());
      }
    } catch (error) {
      emit(ErrorState(error.toString()));
    }
  }

  Student? student;
  Driver? driver;
  Admin? admin;
  Bus? buses;
  Future<void> getProfile({String? id, String? collection}) async {
    try {
      emit(LoadingState());
      var responseData = await FireStoreHelper.getInformationInDocument(
          documentId: id, path: collection);
      if (collection == 'students') {
        student = Student.fromjson(responseData.data()!);
      } else if (collection == 'drivers') {
        driver = Driver.fromjson(responseData.data()!);
      } else if (collection == 'admin') {
        admin = Admin.fromjson(responseData.data()!);
      } else {
        buses = Bus.fromjson(responseData.data()!);
      }
      emit(SuccessState());
    } catch (error) {
      emit(ErrorState(error.toString()));
    }
  }

  Future<void> updateStudent(
      {required String collection,
      required String id,
      required Map<String, dynamic> data}) async {
    try {
      emit(LoadingState());
      await FireStoreHelper.updateDocument(collection, id, data);
      emit(SuccessUpdateState());
    } catch (error) {
      print('hello error $error');
      emit(ErrorState(error.toString()));
    }
  }

  void logout(){

    AppSharedPreferences.removePassword();
    emit(LogoutSuccessfulState());
  }

  void saveLogin(String password, String path, String username, String id){
    AppSharedPreferences.savePassword(password);
    AppSharedPreferences.saveRole(path);
    AppSharedPreferences.saveUsername(username);
    AppSharedPreferences.saveId(id);
  }
}
