import 'package:student_bus_project/Features/Student/Cubit/states.dart';

abstract class AuthState extends OpStates{}

class LogoutLoadingState extends AuthState{}
class LogoutErrorState extends AuthState{}
class LogoutSuccessfulState extends AuthState{}